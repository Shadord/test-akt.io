part of app_theme;

class _ElevatedButtonThemeData extends ElevatedButtonThemeData {
  _ElevatedButtonThemeData()
      : super(
          style: ButtonStyle(
            padding: const EdgeInsets.symmetric(
              vertical: AppDimensions.paddingSmall,
              horizontal: AppDimensions.paddingMedium,
            ).mspAll(),
            foregroundColor: MaterialStateProperty.all<Color>(Colors.white),
            textStyle: const TextStyle(
              fontWeight: FontWeight.w700,
            ).mspAll(),
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(AppDimensions.borderRadius),
            ).mspAll(),
            minimumSize: const Size(10, 10).mspAll<Size>(),
            elevation: MaterialStateProperty.all<double>(0),
          ),
        );
}
