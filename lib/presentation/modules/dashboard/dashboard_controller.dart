import 'package:akt_test/domain/item_group/item_group_entity.dart';
import 'package:akt_test/infrastructure/item/item_repository.dart';
import 'package:get/get.dart';

class DashboardController extends GetxController
    with StateMixin<List<ItemGroupEntity>> {
  DashboardController({
    required this.itemRepository,
  });

  final ItemRepository itemRepository;

  Future<void> fetchItems() async {
    change(null, status: RxStatus.loading());
    List<ItemGroupEntity> newValues = [];
    var cryptoResponse = await itemRepository.findCryptos();
    if (cryptoResponse != null) {
      newValues.add(cryptoResponse);
    }
    var tokenResponse = await itemRepository.findTokens();
    if (tokenResponse != null) {
      newValues.add(tokenResponse);
    }
    // Refetch other datas but for the test it's the same as first ones
    cryptoResponse = await itemRepository.findCryptos();
    if (cryptoResponse != null) {
      newValues.add(cryptoResponse);
    }
    tokenResponse = await itemRepository.findTokens();
    if (tokenResponse != null) {
      newValues.add(tokenResponse);
    }

    change(
      newValues,
      status: newValues.isEmpty ? RxStatus.empty() : RxStatus.success(),
    );
  }

  // ⎧⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎫
  // ⎨ Lifecycle                   ⎬
  // ⎩⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎭

  @override
  Future<void> onInit() async {
    await fetchItems();
    super.onInit();
  }
}
