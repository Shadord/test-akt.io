// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'item_group_entity.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_ItemGroupEntity _$$_ItemGroupEntityFromJson(Map<String, dynamic> json) =>
    _$_ItemGroupEntity(
      title: json['title'] as String,
      items: (json['items'] as List<dynamic>)
          .map((e) => ItemEntity.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$$_ItemGroupEntityToJson(_$_ItemGroupEntity instance) =>
    <String, dynamic>{
      'title': instance.title,
      'items': instance.items,
    };
