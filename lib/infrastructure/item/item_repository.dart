import 'dart:convert';

import 'package:akt_test/domain/item_group/item_group_entity.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/services.dart' show rootBundle;

class ItemRepository with ChangeNotifier {
  Future<ItemGroupEntity?> findCryptos() async {
    try {
      final String dataStr =
          await rootBundle.loadString('assets/mocks/cryptos.json');
      final json = (jsonDecode(dataStr) as Map<String, dynamic>);
      return ItemGroupEntity.fromJson(json);
    } catch (e) {
      return null;
    }
  }

  Future<ItemGroupEntity?> findTokens() async {
    try {
      final String dataStr =
          await rootBundle.loadString('assets/mocks/tokens.json');
      final json = (jsonDecode(dataStr) as Map<String, dynamic>);
      return ItemGroupEntity.fromJson(json);
    } catch (e) {
      return null;
    }
  }
}
