import 'package:akt_test/domain/item_group/item_group_entity.dart';
import 'package:akt_test/presentation/modules/dashboard/widgets/dashboard_item_group_list.dart';
import 'package:flutter/material.dart';

class DashboardContent extends StatelessWidget {
  const DashboardContent({
    required this.itemGroupEntityList,
    Key? key,
  }) : super(key: key);

  final List<ItemGroupEntity> itemGroupEntityList;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: itemGroupEntityList
          .map((e) => DashboardItemGroupList(
                items: e.items,
                title: e.title,
              ))
          .toList(),
    );
  }
}
