import 'package:akt_test/presentation/core/routes/app_routes.dart';
import 'package:akt_test/presentation/modules/dashboard/dashboard_binding.dart';
import 'package:akt_test/presentation/modules/dashboard/dashboard_page.dart';
import 'package:get/get.dart';

class AppPages {
  AppPages._();

  static const initial = AppRoutes.dashboard;

  static final router = [
    GetPage(
      name: AppRoutes.dashboard,
      page: () => const DashboardPage(),
      binding: DashboardBinding(),
    ),
  ];
}
