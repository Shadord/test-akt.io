// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'item_entity.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_ItemEntity _$$_ItemEntityFromJson(Map<String, dynamic> json) =>
    _$_ItemEntity(
      name: json['name'] as String,
      symbol: json['symbol'] as String,
      value: (json['value'] as num).toDouble(),
      delta: (json['daily_delta'] as num).toDouble(),
      color: JSONUtils.colorFromString(json['color'] as String?),
    );

Map<String, dynamic> _$$_ItemEntityToJson(_$_ItemEntity instance) =>
    <String, dynamic>{
      'name': instance.name,
      'symbol': instance.symbol,
      'value': instance.value,
      'daily_delta': instance.delta,
      'color': JSONUtils.colorToString(instance.color),
    };
