import 'package:akt_test/presentation/app.dart';
import 'package:flutter/material.dart';

Future<void> main() async {
  runApp(const App());
}
