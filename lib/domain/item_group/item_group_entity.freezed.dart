// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'item_group_entity.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

ItemGroupEntity _$ItemGroupEntityFromJson(Map<String, dynamic> json) {
  return _ItemGroupEntity.fromJson(json);
}

/// @nodoc
class _$ItemGroupEntityTearOff {
  const _$ItemGroupEntityTearOff();

  _ItemGroupEntity call(
      {required String title, required List<ItemEntity> items}) {
    return _ItemGroupEntity(
      title: title,
      items: items,
    );
  }

  ItemGroupEntity fromJson(Map<String, Object?> json) {
    return ItemGroupEntity.fromJson(json);
  }
}

/// @nodoc
const $ItemGroupEntity = _$ItemGroupEntityTearOff();

/// @nodoc
mixin _$ItemGroupEntity {
  String get title => throw _privateConstructorUsedError;
  List<ItemEntity> get items => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $ItemGroupEntityCopyWith<ItemGroupEntity> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ItemGroupEntityCopyWith<$Res> {
  factory $ItemGroupEntityCopyWith(
          ItemGroupEntity value, $Res Function(ItemGroupEntity) then) =
      _$ItemGroupEntityCopyWithImpl<$Res>;
  $Res call({String title, List<ItemEntity> items});
}

/// @nodoc
class _$ItemGroupEntityCopyWithImpl<$Res>
    implements $ItemGroupEntityCopyWith<$Res> {
  _$ItemGroupEntityCopyWithImpl(this._value, this._then);

  final ItemGroupEntity _value;
  // ignore: unused_field
  final $Res Function(ItemGroupEntity) _then;

  @override
  $Res call({
    Object? title = freezed,
    Object? items = freezed,
  }) {
    return _then(_value.copyWith(
      title: title == freezed
          ? _value.title
          : title // ignore: cast_nullable_to_non_nullable
              as String,
      items: items == freezed
          ? _value.items
          : items // ignore: cast_nullable_to_non_nullable
              as List<ItemEntity>,
    ));
  }
}

/// @nodoc
abstract class _$ItemGroupEntityCopyWith<$Res>
    implements $ItemGroupEntityCopyWith<$Res> {
  factory _$ItemGroupEntityCopyWith(
          _ItemGroupEntity value, $Res Function(_ItemGroupEntity) then) =
      __$ItemGroupEntityCopyWithImpl<$Res>;
  @override
  $Res call({String title, List<ItemEntity> items});
}

/// @nodoc
class __$ItemGroupEntityCopyWithImpl<$Res>
    extends _$ItemGroupEntityCopyWithImpl<$Res>
    implements _$ItemGroupEntityCopyWith<$Res> {
  __$ItemGroupEntityCopyWithImpl(
      _ItemGroupEntity _value, $Res Function(_ItemGroupEntity) _then)
      : super(_value, (v) => _then(v as _ItemGroupEntity));

  @override
  _ItemGroupEntity get _value => super._value as _ItemGroupEntity;

  @override
  $Res call({
    Object? title = freezed,
    Object? items = freezed,
  }) {
    return _then(_ItemGroupEntity(
      title: title == freezed
          ? _value.title
          : title // ignore: cast_nullable_to_non_nullable
              as String,
      items: items == freezed
          ? _value.items
          : items // ignore: cast_nullable_to_non_nullable
              as List<ItemEntity>,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_ItemGroupEntity extends _ItemGroupEntity {
  const _$_ItemGroupEntity({required this.title, required this.items})
      : super._();

  factory _$_ItemGroupEntity.fromJson(Map<String, dynamic> json) =>
      _$$_ItemGroupEntityFromJson(json);

  @override
  final String title;
  @override
  final List<ItemEntity> items;

  @override
  String toString() {
    return 'ItemGroupEntity(title: $title, items: $items)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _ItemGroupEntity &&
            const DeepCollectionEquality().equals(other.title, title) &&
            const DeepCollectionEquality().equals(other.items, items));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(title),
      const DeepCollectionEquality().hash(items));

  @JsonKey(ignore: true)
  @override
  _$ItemGroupEntityCopyWith<_ItemGroupEntity> get copyWith =>
      __$ItemGroupEntityCopyWithImpl<_ItemGroupEntity>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_ItemGroupEntityToJson(this);
  }
}

abstract class _ItemGroupEntity extends ItemGroupEntity {
  const factory _ItemGroupEntity(
      {required String title,
      required List<ItemEntity> items}) = _$_ItemGroupEntity;
  const _ItemGroupEntity._() : super._();

  factory _ItemGroupEntity.fromJson(Map<String, dynamic> json) =
      _$_ItemGroupEntity.fromJson;

  @override
  String get title;
  @override
  List<ItemEntity> get items;
  @override
  @JsonKey(ignore: true)
  _$ItemGroupEntityCopyWith<_ItemGroupEntity> get copyWith =>
      throw _privateConstructorUsedError;
}
