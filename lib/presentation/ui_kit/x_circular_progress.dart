import 'package:akt_test/presentation/ui_kit/theme/app_dimensions.dart';
import 'package:flutter/material.dart';

class XCircularProgress extends StatelessWidget {
  const XCircularProgress({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const Center(
      child: Padding(
        padding: EdgeInsets.all(AppDimensions.paddingBase),
        child: CircularProgressIndicator(),
      ),
    );
  }
}
