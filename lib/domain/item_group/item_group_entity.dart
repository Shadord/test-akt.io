import 'package:akt_test/domain/item/item_entity.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'item_group_entity.freezed.dart';
part 'item_group_entity.g.dart';

@freezed
class ItemGroupEntity with _$ItemGroupEntity {
  const factory ItemGroupEntity({
    required String title,
    required List<ItemEntity> items,
  }) = _ItemGroupEntity;

  factory ItemGroupEntity.fromJson(Map<String, dynamic> json) =>
      _$ItemGroupEntityFromJson(json);

  const ItemGroupEntity._();
}
