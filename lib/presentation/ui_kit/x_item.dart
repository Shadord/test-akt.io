import 'package:akt_test/domain/item/item_entity.dart';
import 'package:akt_test/presentation/ui_kit/theme/app_dimensions.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class XItem extends StatelessWidget {
  const XItem({
    required this.item,
    Key? key,
  }) : super(key: key);

  final ItemEntity item;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(
        right: AppDimensions.paddingThin,
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: [
          SizedBox(
            width: 100,
            height: 100,
            child: Container(
              decoration: BoxDecoration(
                color: item.color,
                borderRadius: BorderRadius.circular(AppDimensions.borderRadius),
              ),
            ),
          ),
          const SizedBox(height: AppDimensions.paddingThin),
          Text(
            item.symbol,
            style: Get.textTheme.bodyText1?.copyWith(
              fontWeight: FontWeight.bold,
            ),
          ),
          Text(
            item.formattedValue,
            style: Get.textTheme.bodyText1?.copyWith(
              color: Colors.white54,
            ),
          ),
          Row(
            children: [
              Icon(
                item.deltaIcon,
                color: item.deltaColor,
                size: AppDimensions.iconSizeThin,
              ),
              const SizedBox(width: 2),
              Text(
                item.formattedDelta,
                style: Get.textTheme.bodyText1?.copyWith(
                  color: item.deltaColor,
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
