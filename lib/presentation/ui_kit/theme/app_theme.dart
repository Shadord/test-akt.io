library app_theme;

import 'package:akt_test/presentation/ui_kit/theme/app_dimensions.dart';
import 'package:akt_test/shared/msp_extensions.dart';
import 'package:flutter/material.dart';

part 'buttons_theme.dart';
part 'text_theme.dart';

class AppTheme {
  static ThemeData? _theme;

  static ThemeData get dark {
    _theme ??= ThemeData(
      primaryColor: Colors.white,
      backgroundColor: Colors.black,
      scaffoldBackgroundColor: Colors.black,
      textTheme: const _TextTheme(),
      elevatedButtonTheme: _ElevatedButtonThemeData(),
    );
    return _theme!;
  }
}
