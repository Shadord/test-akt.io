import 'package:akt_test/presentation/ui_kit/theme/app_dimensions.dart';
import 'package:akt_test/shared/tr_extensions.dart';
import 'package:flutter/material.dart';
import 'package:get/get_core/get_core.dart';
import 'package:get/get_navigation/get_navigation.dart';

class DashboardAppBar extends SliverAppBar {
  DashboardAppBar(
      {required BuildContext context, required String appbarTitle, Key? key})
      : super(
          key: key,
          title: Text(appbarTitle),
          centerTitle: true,
          backgroundColor: Colors.black,
          foregroundColor: Colors.white,
          flexibleSpace: FlexibleSpaceBar(
            centerTitle: true,
            background: Stack(
              children: [
                Image.asset('assets/bg.png'),
                Padding(
                  padding: const EdgeInsets.symmetric(
                    horizontal: AppDimensions.paddingSmall,
                    vertical: AppDimensions.paddingBase,
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      Text(
                        'dashboard_title'.tr,
                        style: Get.textTheme.headline1,
                      ),
                      Text(
                        'dashboard_description'.tr,
                        textAlign: TextAlign.center,
                        style: Get.textTheme.caption?.copyWith(
                          color: Colors.white54,
                        ),
                      ),
                      const SizedBox(height: AppDimensions.paddingSmall),
                      ElevatedButton(
                        onPressed: () {},
                        child: Row(
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            Text('learn_more'.tr),
                            const SizedBox(width: AppDimensions.paddingSmall),
                            const Icon(
                              Icons.arrow_forward_outlined,
                              size: AppDimensions.iconSizeSmall,
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          pinned: true,
          expandedHeight: 400,
        );
}
