import 'package:akt_test/presentation/core/routes/app_pages.dart';
import 'package:akt_test/presentation/ui_kit/theme/app_theme.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class App extends StatelessWidget {
  const App({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      title: "AKT",
      initialRoute: AppPages.initial,
      getPages: AppPages.router,
      theme: AppTheme.dark,
      debugShowCheckedModeBanner: false,
    );
  }
}
