import 'package:akt_test/shared/utils/json_utils.dart';
import 'package:flutter/material.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:intl/intl.dart';

part 'item_entity.freezed.dart';
part 'item_entity.g.dart';

@freezed
class ItemEntity with _$ItemEntity {
  const factory ItemEntity({
    required String name,
    required String symbol,
    required double value,
    @JsonKey(name: 'daily_delta')
        required double delta,
    @JsonKey(
      fromJson: JSONUtils.colorFromString,
      toJson: JSONUtils.colorToString,
    )
        Color? color,
  }) = _ItemEntity;

  factory ItemEntity.fromJson(Map<String, dynamic> json) =>
      _$ItemEntityFromJson(json);

  String get formattedValue => NumberFormat.currency(
        locale: "fr_FR",
        symbol: "€",
        decimalDigits: 2,
      ).format(value);

  bool get positiveDelta => delta >= 0;

  IconData get deltaIcon =>
      positiveDelta ? Icons.north_east_outlined : Icons.south_east_outlined;

  Color get deltaColor => positiveDelta ? Colors.green : Colors.red;

  String get formattedDelta => NumberFormat.currency(
        locale: "fr_FR",
        symbol: "%",
        decimalDigits: 2,
      ).format(delta.abs());

  const ItemEntity._();
}
