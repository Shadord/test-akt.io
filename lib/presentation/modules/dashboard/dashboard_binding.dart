import 'package:akt_test/infrastructure/item/item_repository.dart';
import 'package:akt_test/presentation/modules/dashboard/dashboard_controller.dart';
import 'package:get/get.dart';

class DashboardBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<ItemRepository>(
      () => ItemRepository(),
    );
    Get.put<DashboardController>(
      DashboardController(
        itemRepository: Get.find(),
      ),
    );
  }
}
