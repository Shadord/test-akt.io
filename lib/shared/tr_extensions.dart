import 'package:akt_test/presentation/core/translations/fr_FR_translations.dart';

// For translation management
extension Tr on String {
  String get tr {
    return frFR[this] ?? this;
  }
}
