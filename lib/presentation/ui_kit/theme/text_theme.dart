part of app_theme;

class _TextTheme extends TextTheme {
  const _TextTheme()
      : super(
          bodyText1: const TextStyle(
            color: Colors.white,
            fontSize: 14,
            fontWeight: FontWeight.w400,
          ),
          // bodyText2 -> default to Text widget
          bodyText2: const TextStyle(
            color: Colors.white,
            fontSize: 17,
            fontWeight: FontWeight.w400,
          ),
          caption: const TextStyle(
            color: Colors.white,
            fontSize: 16,
            fontWeight: FontWeight.w400,
          ),
          // Headlines
          headline1: const TextStyle(
            color: Colors.white,
            fontSize: 38,
            fontWeight: FontWeight.w400,
          ),
        );
}
