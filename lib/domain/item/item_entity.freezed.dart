// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'item_entity.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

ItemEntity _$ItemEntityFromJson(Map<String, dynamic> json) {
  return _ItemEntity.fromJson(json);
}

/// @nodoc
class _$ItemEntityTearOff {
  const _$ItemEntityTearOff();

  _ItemEntity call(
      {required String name,
      required String symbol,
      required double value,
      @JsonKey(name: 'daily_delta')
          required double delta,
      @JsonKey(fromJson: JSONUtils.colorFromString, toJson: JSONUtils.colorToString)
          Color? color}) {
    return _ItemEntity(
      name: name,
      symbol: symbol,
      value: value,
      delta: delta,
      color: color,
    );
  }

  ItemEntity fromJson(Map<String, Object?> json) {
    return ItemEntity.fromJson(json);
  }
}

/// @nodoc
const $ItemEntity = _$ItemEntityTearOff();

/// @nodoc
mixin _$ItemEntity {
  String get name => throw _privateConstructorUsedError;
  String get symbol => throw _privateConstructorUsedError;
  double get value => throw _privateConstructorUsedError;
  @JsonKey(name: 'daily_delta')
  double get delta => throw _privateConstructorUsedError;
  @JsonKey(fromJson: JSONUtils.colorFromString, toJson: JSONUtils.colorToString)
  Color? get color => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $ItemEntityCopyWith<ItemEntity> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ItemEntityCopyWith<$Res> {
  factory $ItemEntityCopyWith(
          ItemEntity value, $Res Function(ItemEntity) then) =
      _$ItemEntityCopyWithImpl<$Res>;
  $Res call(
      {String name,
      String symbol,
      double value,
      @JsonKey(name: 'daily_delta')
          double delta,
      @JsonKey(fromJson: JSONUtils.colorFromString, toJson: JSONUtils.colorToString)
          Color? color});
}

/// @nodoc
class _$ItemEntityCopyWithImpl<$Res> implements $ItemEntityCopyWith<$Res> {
  _$ItemEntityCopyWithImpl(this._value, this._then);

  final ItemEntity _value;
  // ignore: unused_field
  final $Res Function(ItemEntity) _then;

  @override
  $Res call({
    Object? name = freezed,
    Object? symbol = freezed,
    Object? value = freezed,
    Object? delta = freezed,
    Object? color = freezed,
  }) {
    return _then(_value.copyWith(
      name: name == freezed
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String,
      symbol: symbol == freezed
          ? _value.symbol
          : symbol // ignore: cast_nullable_to_non_nullable
              as String,
      value: value == freezed
          ? _value.value
          : value // ignore: cast_nullable_to_non_nullable
              as double,
      delta: delta == freezed
          ? _value.delta
          : delta // ignore: cast_nullable_to_non_nullable
              as double,
      color: color == freezed
          ? _value.color
          : color // ignore: cast_nullable_to_non_nullable
              as Color?,
    ));
  }
}

/// @nodoc
abstract class _$ItemEntityCopyWith<$Res> implements $ItemEntityCopyWith<$Res> {
  factory _$ItemEntityCopyWith(
          _ItemEntity value, $Res Function(_ItemEntity) then) =
      __$ItemEntityCopyWithImpl<$Res>;
  @override
  $Res call(
      {String name,
      String symbol,
      double value,
      @JsonKey(name: 'daily_delta')
          double delta,
      @JsonKey(fromJson: JSONUtils.colorFromString, toJson: JSONUtils.colorToString)
          Color? color});
}

/// @nodoc
class __$ItemEntityCopyWithImpl<$Res> extends _$ItemEntityCopyWithImpl<$Res>
    implements _$ItemEntityCopyWith<$Res> {
  __$ItemEntityCopyWithImpl(
      _ItemEntity _value, $Res Function(_ItemEntity) _then)
      : super(_value, (v) => _then(v as _ItemEntity));

  @override
  _ItemEntity get _value => super._value as _ItemEntity;

  @override
  $Res call({
    Object? name = freezed,
    Object? symbol = freezed,
    Object? value = freezed,
    Object? delta = freezed,
    Object? color = freezed,
  }) {
    return _then(_ItemEntity(
      name: name == freezed
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String,
      symbol: symbol == freezed
          ? _value.symbol
          : symbol // ignore: cast_nullable_to_non_nullable
              as String,
      value: value == freezed
          ? _value.value
          : value // ignore: cast_nullable_to_non_nullable
              as double,
      delta: delta == freezed
          ? _value.delta
          : delta // ignore: cast_nullable_to_non_nullable
              as double,
      color: color == freezed
          ? _value.color
          : color // ignore: cast_nullable_to_non_nullable
              as Color?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_ItemEntity extends _ItemEntity {
  const _$_ItemEntity(
      {required this.name,
      required this.symbol,
      required this.value,
      @JsonKey(name: 'daily_delta')
          required this.delta,
      @JsonKey(fromJson: JSONUtils.colorFromString, toJson: JSONUtils.colorToString)
          this.color})
      : super._();

  factory _$_ItemEntity.fromJson(Map<String, dynamic> json) =>
      _$$_ItemEntityFromJson(json);

  @override
  final String name;
  @override
  final String symbol;
  @override
  final double value;
  @override
  @JsonKey(name: 'daily_delta')
  final double delta;
  @override
  @JsonKey(fromJson: JSONUtils.colorFromString, toJson: JSONUtils.colorToString)
  final Color? color;

  @override
  String toString() {
    return 'ItemEntity(name: $name, symbol: $symbol, value: $value, delta: $delta, color: $color)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _ItemEntity &&
            const DeepCollectionEquality().equals(other.name, name) &&
            const DeepCollectionEquality().equals(other.symbol, symbol) &&
            const DeepCollectionEquality().equals(other.value, value) &&
            const DeepCollectionEquality().equals(other.delta, delta) &&
            const DeepCollectionEquality().equals(other.color, color));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(name),
      const DeepCollectionEquality().hash(symbol),
      const DeepCollectionEquality().hash(value),
      const DeepCollectionEquality().hash(delta),
      const DeepCollectionEquality().hash(color));

  @JsonKey(ignore: true)
  @override
  _$ItemEntityCopyWith<_ItemEntity> get copyWith =>
      __$ItemEntityCopyWithImpl<_ItemEntity>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_ItemEntityToJson(this);
  }
}

abstract class _ItemEntity extends ItemEntity {
  const factory _ItemEntity(
      {required String name,
      required String symbol,
      required double value,
      @JsonKey(name: 'daily_delta')
          required double delta,
      @JsonKey(fromJson: JSONUtils.colorFromString, toJson: JSONUtils.colorToString)
          Color? color}) = _$_ItemEntity;
  const _ItemEntity._() : super._();

  factory _ItemEntity.fromJson(Map<String, dynamic> json) =
      _$_ItemEntity.fromJson;

  @override
  String get name;
  @override
  String get symbol;
  @override
  double get value;
  @override
  @JsonKey(name: 'daily_delta')
  double get delta;
  @override
  @JsonKey(fromJson: JSONUtils.colorFromString, toJson: JSONUtils.colorToString)
  Color? get color;
  @override
  @JsonKey(ignore: true)
  _$ItemEntityCopyWith<_ItemEntity> get copyWith =>
      throw _privateConstructorUsedError;
}
