import 'package:akt_test/presentation/modules/dashboard/dashboard_controller.dart';
import 'package:akt_test/presentation/modules/dashboard/widgets/dashboard_app_bar.dart';
import 'package:akt_test/presentation/modules/dashboard/widgets/dashboard_content.dart';
import 'package:akt_test/presentation/ui_kit/x_circular_progress.dart';
import 'package:akt_test/shared/tr_extensions.dart';
import 'package:flutter/material.dart';
import 'package:get/get_state_manager/get_state_manager.dart';

class DashboardPage extends GetView<DashboardController> {
  const DashboardPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: RefreshIndicator(
          onRefresh: controller.fetchItems,
          child: CustomScrollView(
            slivers: [
              DashboardAppBar(
                context: context,
                appbarTitle: 'dashboard_appbar_title'.tr,
              ),
              SliverList(
                delegate: SliverChildListDelegate(
                  [
                    controller.obx(
                      (state) => DashboardContent(
                        itemGroupEntityList: state ?? [],
                      ),
                      onLoading: const XCircularProgress(),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
