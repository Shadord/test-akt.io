# AKT.io TEST


# This project uses:

- [GetX](https://pub.dev/packages/get) - Pour le State Management et l'injection des dépendances.
- [Freezed](https://pub.dev/packages/freezed) — Permet de générer des classes qui contiennent les fonctions principales comme toString, fromJson, toJson, permet l'utilisation d'un DTO depuis l'entité directement.

# Table of Contents

- [1. Quick start](#1-quick-start-anchor)

# 1. Quick start :anchor:

1. Flutter version

   ```bash
   flutter doctor -v
   ```

   Here is the version of flutter and its tools that I use:

   ```raw
   [√] Flutter (Channel stable, 2.10.2, on Microsoft Windows [version 10.0.19042.1466], locale fr-FR)
    • Flutter version 2.10.2 at C:\Users\natha\Flexper\flutter
    • Upstream repository https://github.com/flutter/flutter.git
    • Framework revision 097d3313d8 (9 days ago), 2022-02-18 19:33:08 -0600
    • Engine revision a83ed0e5e3
    • Dart version 2.16.1
    • DevTools version 2.9.2
   ```

2. Install dependencies

   ```bash
   flutter pub get
   ```

5. Run the app (dev env)

   ```bash
   open -a Simulator
   flutter run
   ```

# 2. Précisions sur le test
N'ayant actuellement pas de Mac, j'ai pu effectuer les tests seulement sur Android.
   
Un item_entity est un objet qui correspond soit à un token, soit a une crypto.
Un item_group_entity, est un groupe de tokens / cryptos.

Pour les couleurs, elles sont randomisées à chaque fetching dans le json_utils.dart.
Il y a un RefreshIndicator pour randomizer les couleurs.
      

J'ai utilisé GetX qui est un package doté de multiples fonctionnalités.
D'autres alternatives sont possibles avec des fonctions natives de Flutter comme ChangeNotifier / Provider et Flutter_Bloc
    
Pour le système tr_extention, c'est une alternative à Get_Translate pour la traduction des items et donc le changement de langage
    

En vous souhaitant bonne lecture.
Nathan