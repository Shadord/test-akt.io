import 'package:akt_test/domain/item/item_entity.dart';
import 'package:akt_test/presentation/ui_kit/theme/app_dimensions.dart';
import 'package:akt_test/presentation/ui_kit/x_item.dart';
import 'package:flutter/material.dart';
import 'package:get/get_core/get_core.dart';
import 'package:get/get_navigation/get_navigation.dart';

class DashboardItemGroupList extends StatelessWidget {
  const DashboardItemGroupList({
    required this.items,
    required this.title,
    Key? key,
  }) : super(key: key);

  final List<ItemEntity> items;
  final String title;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        Padding(
          padding: const EdgeInsets.only(
            left: AppDimensions.paddingSmall,
          ),
          child: Text(
            title,
            style: Get.textTheme.caption?.copyWith(
              color: Colors.white54,
            ),
          ),
        ),
        SingleChildScrollView(
          scrollDirection: Axis.horizontal,
          padding: const EdgeInsets.all(AppDimensions.paddingSmall),
          child: Row(
            children: items.map((e) => XItem(item: e)).toList(),
          ),
        ),
        const SizedBox(height: AppDimensions.paddingSmall),
      ],
    );
  }
}
