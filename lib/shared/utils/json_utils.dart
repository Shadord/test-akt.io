import 'dart:math';

import 'package:flutter/material.dart';

abstract class JSONUtils {
  static Color? colorFromString(String? str) {
    // I choose to have a random color instead of randomize in mocked json
    return Colors.primaries[Random().nextInt(Colors.primaries.length)];
  }

  static String? colorToString(Color? c) => c?.toString();
}
